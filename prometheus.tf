 resource "google_compute_instance" "prometheus" {
  name         = "${var.name}-prometheus-0"
  zone         = "${var.zone}"
  machine_type = "${var.machine_type}"

  boot_disk {
    initialize_params {
      image = "${var.source_image}"
    }
  }

  network_interface {
    subnetwork    = "${google_compute_subnetwork.subnet.self_link}"
    access_config = {}
  }

  connection {
    user        = "jcampbell"
    type        = "ssh"
    private_key = "${file(var.ssh_key)}"
    timeout     = "2m"
  }

  provisioner "file" {
    source      = "prometheusserver.sh"
    destination = "/tmp/prometheusserver.sh"
  }

  provisioner "file" {
    source      = "grafanaserver.sh"
    destination = "/tmp/grafanaserver.sh"
  }

  provisioner "remote-exec" {
    inline = [
      "export PATH=$PATH:/usr/bin",
      "sudo apt install golang-go -y",
      "wget https://github.com/prometheus/prometheus/releases/download/v2.9.2/prometheus-2.9.2.linux-amd64.tar.gz -d /tmp/prometheus-2.9.2.linux-amd64.tar.gz",
      "sudo chmod +x /tmp/prometheusserver.sh",
      "sudo /tmp/./prometheusserver.sh",
      "sudo chmod +x /tmp/grafanaserver.sh",
      "sudo /tmp/./grafanaserver.sh",
    ]
  }

  provisioner "local-exec" {
      command = "open http://${google_compute_instance.prometheus.network_interface.0.access_config.0.nat_ip}:9090 && open http://${google_compute_instance.prometheus.network_interface.0.access_config.0.nat_ip}:3000"
  }
}

output "prometheus-ip" {
  value = "${google_compute_instance.prometheus.*.network_interface.0.access_config.0.nat_ip}"
}

